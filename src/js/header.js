const buttonToOpen = document.querySelector(".navbar-menu_logo");


function openMenu() {
    const menu = document.querySelector(".header__menu");
    let dst = getComputedStyle(menu).display;
    if (dst == 'none') {
        buttonToOpen.innerHTML = '<img src=" ./dist/img/first_section/Vector 3.png" alt="close" width="26" />';
        menu.style.display = 'block';
    }
    else {
        menu.style.display = 'none';
        buttonToOpen.innerHTML = '<img src=" ./dist/img/first_section/burger.png" alt="burgrer" width="26" />';
    }
}


buttonToOpen.addEventListener("click", openMenu)
